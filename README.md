 **- INSTALL**
 - git clone https://rodrigomrosa@bitbucket.org/rodrigomrosa/back-end-movies.git
 - cd /projetct folder
 - cp .env.exemple .env 
 - configue .env database infos
 - composer install 
 - php artisan migrate
 - register domian in nginx, apache, etc.

 **- API DOCUMENTATION**
 - Auth
 payload
	
 {
	"email":"string",
	"password": "string"
}

Exemple return 200

  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9tb3ZpZXMuYXBpXC9hcGlcL2F1dGhcL2xvZ2luIiwiaWF0IjoxNTc4NjgzNzk2LCJleHAiOjE1Nzg2ODczOTYsIm5iZiI6MTU3ODY4Mzc5NiwianRpIjoiYVhMcURoZnVma3cxZ2drcCIsInN1YiI6MSwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9._yCj0_o5Tt_sVEKEh1tSNsMnkVJ8JYYKmpRBytzYIH4",
  "token_type": "bearer",
  "expires_in": 3600
}

Endpoints (POST, GET, PUT)
Headers
Authorizathion: bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9tb3ZpZXMuYXBpXC9hcGlcL2F1dGhcL2xvZ2luIiwiaWF0IjoxNTc4NjgzNzk2LCJleHAiOjE1Nzg2ODczOTYsIm5iZiI6MTU3ODY4Mzc5NiwianRpIjoiYVhMcURoZnVma3cxZ2drcCIsInN1YiI6MSwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9._yCj0_o5Tt_sVEKEh1tSNsMnkVJ8JYYKmpRBytzYIH4

- Movies
payloads  (POST)
{
	"name": string
	"file": path
}

- Tags:
{
	"name": string
}

