<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;
use App\User;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Str;
use App\Models\Tag;
use App\Models\TagRelationship;



class MovieController extends Controller
{
    //
    public function create(Request $request)
    {
        try {
            $user = User::where('email', auth()->user()->email)->first();

            $extension = $request->file('file')->getClientOriginalExtension();

            $file_size = $request->file('file')->getSize();
            $file_size = number_format($file_size / 1048576,2);


            if($extension == 'avi' || $extension == 'mov' || $extension == 'wmv' || $extension == 'mp4' || $extension == 'flv'
                || $extension == 'mkv') {

                if($file_size > 5) {

                    return response([
                        'message' => trans('Arquivo superior a 5MB'),
                    ], Response::HTTP_OK);

                } else {

                    Movie::create([
                        'user_id' => $user->id,
                        'name' => $request->name,
                        'file_size' => $file_size.'M',
                    ]);

                    $movie = Movie::orderBy('id', 'desc')->first();

                    $nameFile = $movie->id . ".{$extension}";

                    $path = "movies/";
                    $request->file('file')->move(public_path().'/'.$path, $nameFile);

                    Movie::where('id', $movie->id)->update([
                       'path' =>  $path.'/'.$nameFile
                    ]);

                    $tags  = explode(',', $request->tags);

                    foreach ($tags as $tag) {

                        TagRelationship::create([
                            'movie_id' => $movie->id,
                            'tag_id' => $tag,
                        ]);
                    }

                    return response([
                        'message' => trans('Cadastro realizado com sucesso'),
                    ], Response::HTTP_OK);
                }

            } else {

                return response([
                    'message' => trans('Arquivo de formato não suportado'),
                ], Response::HTTP_OK);
            }

        } catch (\Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function update(Request $request)
    {
        try {
            Movie::where('id', $request->id)->update([
                'name' => $request->name,
            ]);

            return response([
                'message' => trans('Nome alterado com sucesso'),
            ], Response::HTTP_OK);

        } catch (\Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function getMovies()
    {
        try {
            $movies = Movie::with('user')->get();

            return response([
                'movies' => $movies,
            ], Response::HTTP_OK);

        } catch (\Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function deleteMovie($id)
    {
        try {
            Movie::where('id', $id)->delete();

            TagRelationship::where('movie_id', $id)->delete();

            return response([
                'message' => trans('Filme excluído com sucesso!'),
            ], Response::HTTP_OK);

        } catch (\Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }

    }


    public function getTags()
    {
        try {
            $tags = Tag::get();

            return response([
                'tags' => $tags,
            ], Response::HTTP_OK);


        } catch (\Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }

    }

    public function getTagMovie(Request $request)
    {
        try {
            $tags = TagRelationship::where('movie_id', $request->id)
                ->with('tag')
                ->get();

            return response([
                'tags' => $tags,
            ], Response::HTTP_OK);

        } catch (\Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function deteleTagMovie($id)
    {
        try {
            TagRelationship::where('id', $id)->delete();

            return response([
                'message' => trans('Tag excluída com sucesso!'),
            ], Response::HTTP_OK);

        } catch (\Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }

    }

    public function insertTagMovie(Request $request)
    {
        try {
            $tags  = explode(',', $request->tags);

            foreach ($tags as $tag) {

                TagRelationship::create([
                    'movie_id' => $request->id,
                    'tag_id' => $tag,
                ]);
            }

            return response([
                'message' => trans('Tag inserida com sucesso'),
            ], Response::HTTP_OK);


        } catch (\Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function createTag(Request $request)
    {
        try {
            Tag::create([
                'name' => $request->name,
            ]);

            return response([
                'message' => trans('Cadastro realizado com sucesso'),
            ], Response::HTTP_OK);


        } catch (\Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }

    }

}
