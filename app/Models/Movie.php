<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Movie extends Model
{
    //
    protected $fillable = [
        'user_id', 'name', 'path', 'file_size',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
