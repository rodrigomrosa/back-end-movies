<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Movie;
use App\Models\Tag;

class TagRelationship extends Model
{
    //
    protected $fillable = [
        'movie_id', 'tag_id',
    ];


    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
}
