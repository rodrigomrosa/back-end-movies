<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function ($router){
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

    Route::get('user', 'User\UserController@index');

    Route::get('movies', 'MovieController@getMovies');
    Route::post('movie/create', 'MovieController@create');
    Route::any('movie/update', 'MovieController@update');
    Route::any('movie/delete/{id}', 'MovieController@deleteMovie');

    Route::any('tags/movie', 'MovieController@getTagMovie');
    Route::any('tags/movie/delete/{id}', 'MovieController@deteleTagMovie');

    Route::post('movie/tags/insert', 'MovieController@insertTagMovie');



    Route::get('tags', 'MovieController@getTags');
    Route::post('tag/create', 'MovieController@createTag');





});

Route::post('register', 'User\RegisterController@register');

